<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// 
///  Fichier produit par PlugOnet
// Module: paquet-squelpriv
// Langue: fr
// Date: 17-03-2023 13:00:00
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'squelpriv_description' => 'La présentation générale de Spip n\'est pas modifiée. Le jeu de squelettes ne modifie que la navigation dans l\'espace privé ce qui rend le contenu de l\'espace privé plus lisible et accessible sur smartphone.
    
    Général :
    - l\'icône de l\'auteur fait apparaître au survol un dropdown contenant les infos de l\'auteur et la déconnexion.
    - le texte \"Espace privé\" est cliquable et ramène à la page d\'accueil de l\'espace privé.
    - l\'icône du site est cliquable pour accéder à l\'interface public.
    - une flèche apparait en bas de page lors du défilement de la page pour directement revenir en haut. 

    Sur petits écrans style smartphone : 
    - la barre de navigation s\'adapte au format de l\'écran avec un bouton menu. 
    - les colonnes navigation et extras sont dans des blocs dépliables. Le bloc contenant les infos, docs,... est déplié par défaut à l\'ouverture d\'une page. 
    
    Les librairies tailwindcss sont intégrées en CDN :
    - tailwind CSS et elements : dernière version stable
    - flowbite : 1.6.4 

    A faire: internationaliser 
    ',
	'squelpriv_slogan' => 'Squelettes Zcore + tailwindcss et elements + Flowbite + HTML5',
);
?>