<?php
/**
 * Squelette squelpriv
 * (c) 2023 Licence GPL 3
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

function squelpriv_header_prive($flux){
	$flux .= "<!--squelpriv css et script-->
	<link rel=\"stylesheet\" href=\"/plugins/auto/squelpriv/v2.0.1/css/tailwind.css\" type=\"text/css\">
	<style>
    @media (min-width:1024px){
		body {
			padding-top:120px !important;
		}
    #page h1{
      padding-bottom:1em;
    }
  }
  @media (max-width:1024px){
		body {
			padding-top:60px !important;
		}
  }
    #page h1{
      font-weight:bold;
      font-variant: small-caps;
      font-size:1.3em;
    }
	</style>
	<!--fin insertion squelpriv-->
	";
	return $flux;
}

function upper ($chaine) {
	return strtoupper($chaine);
}


?>
